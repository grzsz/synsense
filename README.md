## Due to technical issues project was checked on Ubuntu 20.04,
gcc version: 9.3.0

## How to build:
cmake . && make

## Build output:
Two files in folder ./bin:
EFS - main binary
EFS_test - unit tests for EFS

## Problem description
Although in real life it would be a part of larger system with input and
output streams, goal of this exercise is to take a collection of events,
process the collection by transforming, adding and/or removing elements and
return the collection.

## Design decisions
0) Task is very general and only few use cases are known, so solution is
made to be simple but easily extensible.

1) Filters are placed outside of main EFS class so new ones can be easily
added.
Pros:
	Very simple structure of main EFS class
	Very easy to add new filters
Cons:
	Filters are responsible for data integrity
	Filters are responsible for data order

2) Result is a modified input.
Pros:
	Less memory usage, no additional allocations
	Easy chaining without copying/moving
Cons:
	Collection has to exist until processing is finished (multithreading)
	May be less readable

3) EFS supports filter chains by executing filters (in the order in which
they were added) on the same event collection
Pros:
	Clean static configuration with not many filters
	Extensible - new options (like parallel processing) can be easily
	added in the future. Setup could look like:
	efs.AddFilter(f1)
		.StartParallel()
			.AddFilter(f2)
			.AddFilter(f3)
		.EndParallel(MergeStrategy::Common)
		.AddFilter(f4);
Cons:
	Less readable adding 100s of filters (for example through a loop)

4) Multithreading: filtering can be run asynchronously.
Because of most obvious use case, whole chain is run in a separate thread.
For edge cases, like running part of the chain in parallel, extensions
are required.
Pros:
	Simple solution
Cons:
	May not fulfill some edge use cases (see example in point 3)
