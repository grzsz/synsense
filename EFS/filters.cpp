#include "filters.h"

#include <algorithm>

// Divide all timestamps by 10
void filterFixTimestamps(EFS::EventCollection& collection, EFS::Predicate) {
	for (auto& ev : collection) {
		ev.timestamp /= 10;
	}
}

// Duplicate every second event; duplicated event has unique id
void filterDuplicate(EFS::EventCollection& collection, EFS::Predicate) {
	uint64_t firstAvailableId = collection.rbegin()->id + 1;
	size_t originalSize = collection.size();
	for (size_t i = 0; i < originalSize; i += 2) {
		collection.push_back({ firstAvailableId++, collection[i].timestamp });
	}
	std::sort(collection.begin(), collection.end(), [](const EFS::Event& ev1, const EFS::Event ev2) { return ev1.timestamp < ev2.timestamp; });
}

// Removes events with given predicate
void filterRemove(EFS::EventCollection& collection, EFS::Predicate p) {
	auto it = std::remove_if(collection.begin(), collection.end(), p);
	collection.erase(it, collection.end());
}
