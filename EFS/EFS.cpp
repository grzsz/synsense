﻿#include "EFS.h"

namespace EFS {

EventFilteringSystem& EventFilteringSystem::AddFilter(ProcessingFunction filter, Predicate predicate) {
	filters.push_back({ filter, predicate });
	return *this;
}

EventFilteringSystem& EventFilteringSystem::ClearFilters() {
	filters.clear();
	return *this;
}

bool EventFilteringSystem::Filter(EventCollection& collection) {
	for (auto& filter : filters) {
		if (filter.filter) {
			filter.filter(collection, filter.predicate);
		}
	}
	return true;
}

std::future<bool> EventFilteringSystem::FilterAsync(EventCollection& collection) {
	return std::async(std::launch::async, &EventFilteringSystem::Filter, this, std::ref(collection));
}

} // ~namespace EFS
