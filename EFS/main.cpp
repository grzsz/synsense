#include "EFS.h"
#include "filters.h"

#include <vector>
#include <iostream>
#include <algorithm>

using std::vector;
using std::cout;
using Efs = EFS::EventFilteringSystem;

std::vector<EFS::Event> generateEvents() {
	std::vector<EFS::Event> events;
	for (uint32_t i = 0; i < 10; i++) {
		events.push_back({ i, 10 * i });
	}
	return events;
}

std::vector<EFS::Event> generateManyEvents() {
	std::vector<EFS::Event> events;
	for (uint32_t i = 0; i < 100000; i++) {
		events.push_back({ (uint64_t)i + 10000, i * 10 });
	}
	return events;
}

int main()
{
	using namespace std::placeholders;

	Efs efs;

	efs.AddFilter(filterFixTimestamps)
		.AddFilter(filterDuplicate)
		.AddFilter(filterRemove, [](EFS::Event& ev) {
		return ev.timestamp < 5;
			});


	// Synchronous processing
	auto collection = generateEvents();
	efs.Filter(collection);

	cout << "Results of processed collection:\n";
	for (const auto& ev : collection) {
		cout << "id: " << ev.id << " timestamp: " << ev.timestamp << '\n';
	}
	cout << "===========\n";

	// Asynchronous processing
	auto largeCollection = generateManyEvents();
	auto isReadyFuture = efs.FilterAsync(largeCollection);
	// Wait for async processing to finish
	isReadyFuture.get();
	cout << "First few results of asnychronously processed large collection:\n";
	for (int i = 0; i < 5; i++) {
		cout << "id: " << largeCollection[i].id << " timestamp: " << largeCollection[i].timestamp << '\n';
	}

	return 0;
}
