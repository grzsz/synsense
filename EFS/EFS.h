﻿#pragma once
#include <cstdint>
#include <vector>
#include <future>

namespace EFS {

struct Event {
	uint64_t id = 0;
	uint32_t timestamp = 0;
};

using EventCollection = std::vector<Event>;
using Predicate = bool(*)(Event&);
using ProcessingFunction = void(*)(EventCollection&, Predicate);

class EventFilteringSystem {
public:
	EventFilteringSystem() {}

	EventFilteringSystem(const EventFilteringSystem&) = delete;
	EventFilteringSystem& operator=(const EventFilteringSystem&) = delete;

	virtual ~EventFilteringSystem() {}

	EventFilteringSystem& AddFilter(ProcessingFunction filter, Predicate predicate = nullptr);
	EventFilteringSystem& ClearFilters();
	bool Filter(EventCollection& collection);
	std::future<bool> FilterAsync(EventCollection& collection);

protected:
	struct FilterWithPredicate {
		ProcessingFunction filter;
		Predicate predicate;
	};
	std::vector<FilterWithPredicate> filters;
};

} // ~namespace EFS