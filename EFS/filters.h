#pragma once

#include "EFS.h"

#include <vector>

// Divide all timestamps by 10
void filterFixTimestamps(EFS::EventCollection& collection, EFS::Predicate);

// Duplicate every second event; duplicated event has unique id
void filterDuplicate(EFS::EventCollection& collection, EFS::Predicate);

// Removes events with given predicate
void filterRemove(EFS::EventCollection& collection, EFS::Predicate p);
