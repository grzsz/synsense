#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "EFS.h"

#include <thread>
#include <chrono>

using namespace std::chrono_literals;


class EFS_test : public EFS::EventFilteringSystem {
public:
	using EventFilteringSystem::filters;
};

void filterTestEmpty(EFS::EventCollection& collection, EFS::Predicate) {
}

void filterTestModifyTimestamp(EFS::EventCollection& collection, EFS::Predicate) {
	for (auto& ev : collection) {
		ev.timestamp /= 10;
	}
}

std::mutex filterMutex;
void filterTestModifyTimestampLock(EFS::EventCollection& collection, EFS::Predicate) {
	std::unique_lock<std::mutex> lock(filterMutex);

	for (auto& ev : collection) {
		ev.timestamp /= 10;
	}
}

TEST_CASE("Add single filter", "[filter_add]") {
	EFS_test efs;
	efs.AddFilter(filterTestEmpty);
	REQUIRE(efs.filters.size() == 1);
	efs.ClearFilters();
	REQUIRE(efs.filters.size() == 0);
}

TEST_CASE("Add two filters", "[filter_add]") {
	EFS_test efs;
	efs.AddFilter(filterTestEmpty).AddFilter(filterTestEmpty);
	REQUIRE(efs.filters.size() == 2);
	efs.ClearFilters();
	REQUIRE(efs.filters.size() == 0);
}

TEST_CASE("Modify elements in the collection", "[filter_collection]") {
	EFS_test efs;
	efs.AddFilter(filterTestModifyTimestamp);
	
	EFS::EventCollection collection{ {1, 10}, {2, 20} };
	efs.Filter(collection);

	REQUIRE(collection[0].timestamp == 1);
	REQUIRE(collection[1].timestamp == 2);
}

TEST_CASE("Modify using null filter", "[filter_collection]") {
	EFS_test efs;
	efs.AddFilter(nullptr);

	EFS::EventCollection collection{ {1, 10}, {2, 20} };
	efs.Filter(collection);

	REQUIRE(collection[0].timestamp == 10);
	REQUIRE(collection[1].timestamp == 20);
}

TEST_CASE("Modify elements in the collection async", "[filter_async]") {
	EFS_test efs;
	efs.AddFilter(filterTestModifyTimestampLock);

	EFS::EventCollection collection{ {1, 10}, {2, 20} };
	filterMutex.lock();
	auto ready = efs.FilterAsync(collection);
	std::this_thread::sleep_for(100ms);
	REQUIRE(collection[0].timestamp == 10);
	REQUIRE(collection[1].timestamp == 20);
	filterMutex.unlock();
	ready.get();
	REQUIRE(collection[0].timestamp == 1);
	REQUIRE(collection[1].timestamp == 2);

}
